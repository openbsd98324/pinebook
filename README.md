```
Specifications

    Brand: Pinebook
    Storage: 64GB/128GB eMMC
    CPU: 4x ARM Cortex A53, 2x ARM Cortex A72
    Memory: 4GB LPDDR4 RAM
    Operating System: Linux (ARM)
    Battery: 10,000mAh
    Ports: USB 3.0, USB 2.0, USB Type-C, MicroSD, 3.5mm jack
    Camera: 2MP
    Display (Size, Resolution): 14" Full HD (1920x1080)
    Weight: 1.26kg (2.78lbs)
```



![](media/PinebookMain.jpg)


## Pinebook pro

Firmware + modules 5.18

https://gitlab.com/openbsd98324/linux-kernel-collection/-/raw/main/lib-pinebook-pro-modules+firmware-only-202301-manjaro-5.18.0-4-MANJARO-ARM-min-modules-firmware-kdedesk-mobile-v1.2.tar.gz





# Manjaro


 https://mirrors.gigenet.com/OSDN//storage/g/m/ma/manjaro-arm/pinebook/xfce/20.12/Manjaro-ARM-xfce-pinebook-20.12.img.xz7


 https://free.nchc.org.tw/osdn//storage/g/m/ma/manjaro-arm/pinebook/kde-plasma/20.12/Manjaro-ARM-kde-plasma-pinebook-20.12.img.xz




## Boot from USB


First step is to download : 
[url]https://ftp.halifax.rwth-aachen.de/osdn/storage/g/m/ma/manjaro-arm/pinebook/xfce/20.12/Manjaro-ARM-xfce-pinebook-20.12.img.xz[/url]

xzcat Manjaro-ARM-xfce-pinebook-20.12.img.xz > /dev/sda 

dd if=/dev/zero of=/dev/sda1

After that, you go on the pinebook and change the root to /dev/sda2, it will look like.
mount /dev/mmcblk2p2 /mnt
or edit /boot directory

[CODE]
LABEL Manjaro ARM
KERNEL /Image
FDT /dtbs/rockchip/rk3399-pinebook-pro.dtb
APPEND initrd=/initramfs-linux.img console=ttyS2,1500000 root=/dev/sda2 rw rootwait audit=0 splash plymouth.ignore-serial-consoles
[/CODE]

This requires no opening of the pinebook and changing the switch eMMC.

